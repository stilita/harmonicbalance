import numpy as np
import matplotlib.pyplot as plt


Re = np.array([130.0, 140, 150, 160, 170, 200])

alpha = np.array([-10.0, -5.0, -3.0, -1.0, 0.0, 1.0, 3.0, 5.0, 10.0])

xv, yv = np.meshgrid(alpha, Re)

cl = np.zeros_like(xv)
cd = np.zeros_like(xv)


cd[0, :] = [1.72, 1.49, 1.44, 1.41, 1.41, 1.41, 1.44, 1.49, 1.72]
cl[0, :] = [0.077, 0.084, 0.058, 0.02, 0.0, -0.02, -0.058, -0.084, -0.077]

cd[1, :] = [1.75, 1.49, 1.44, 1.41, 1.41, 1.41, 1.44, 1.49, 1.75]
cl[1, :] = [0.089, 0.101, 0.071, 0.025, 0.0, -0.025, -0.071, -0.101, -0.089]

cd[2, :] = [1.77, 1.50, 1.44, 1.41, 1.41, 1.41, 1.44, 1.50, 1.77]
cl[2, :] = [0.099, 0.118, 0.085, 0.030, 0.0, -0.030, -0.085, -0.118, -0.099]

cd[3, :] = [1.80, 1.50, 1.44, 1.42, 1.41, 1.42, 1.44, 1.50, 1.80]
cl[3, :] = [0.111, 0.136, 0.098, 0.036, 0.0, -0.036, -0.098, -0.136, -0.111]

cd[4, :] = [1.83, 1.51, 1.45, 1.42, 1.42, 1.42, 1.45, 1.51, 1.83]
cl[4, :] = [0.123, 0.152, 0.111, 0.041, 0.0, -0.041, -0.111, -0.152, -0.123]

cd[5, :] = [1.91, 1.54, 1.47, 1.44, 1.44, 1.44, 1.47, 1.54, 1.91]
cl[5, :] = [0.167, 0.200, 0.150, 0.055, 0.0, -0.055, -0.150, -0.20, -0.167]

#print(cd)

#print(cl)

""" contour_levels = np.linspace(1.4, 2.0, 31)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 5))

CF = ax1.contourf(xv, yv, cd, levels=contour_levels, cmap='viridis')
# contours = ax1.contour(xv, yv, cd, levels=contour_levels, colors=['b'])
ax1.set_xlabel(r"$\alpha$")
ax1.set_ylabel(r"$Re$")
ax1.set_title("$C_d$")
cbar = fig.colorbar(CF, ax=ax1)
# cbar.set_ticks(contour_levels)
# cbar.set_label('lateral acceleration margin')

contour_levels = np.linspace(-0.2, 0.2, 31)

CF = ax2.contourf(xv, yv, cl, levels=contour_levels, cmap='plasma')
# contours = ax1.contour(xv, yv, cd, levels=contour_levels, colors=['b'])
ax2.set_xlabel(r"$\alpha$")
ax2.set_ylabel(r"$Re$")
ax2.set_title("$C_l$")
cbar = fig.colorbar(CF, ax=ax2)


plt.savefig('cdcl.png', bbox_inches='tight')
plt.show()
 """
x = np.deg2rad(alpha)
cd1 = cd[-1,:]
cl1 = cl[-1,:]

coeffs_cd = np.polyfit(x, cd1, 2)
coeffs_cl = np.polyfit(x, cl1, 3)

print(coeffs_cd)
print(coeffs_cl)

cd0 = coeffs_cd[-1]
cd_alpha2 = coeffs_cd[-3]

cl_alpha = coeffs_cl[-2]
cl_alpha3 = coeffs_cl[-4]

print(f"cd0 = {cd0}")
print(f"cd_alpha2 = {2*cd_alpha2}")
print(f"cl_alpha = {cl_alpha}")
print(f"cl_alpha3 = {6*cl_alpha3}")

# Generate fitted polynomial using np.polyval
fitted_cd = np.polyval(coeffs_cd, x)
fitted_cl = np.polyval(coeffs_cl, x)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 5))
ax1.set_xlabel(r"$\alpha$")
ax1.set_ylabel(r"$c_d$")
ax1.plot(x, cd1, 'o', label='Data')
ax1.plot(x, fitted_cd, label='4th Order Even Polynomial Fit')
ax1.legend()

ax2.set_xlabel(r"$\alpha$")
ax2.set_ylabel(r"$c_l$")
ax2.plot(x, cl1, 'o', label='Data')
ax2.plot(x, fitted_cl, label='3th Order odd Polynomial Fit')
ax2.legend()

plt.savefig('fitted.png', bbox_inches='tight')

